const Estudiante = require('../Models/estudiante');
const estudianteMateria = require('../Models/estudianteMateria');

const estudianteCtrl = {};

estudianteCtrl.getEstudiantes = async (req,res) => {
    const estudiantes = await Estudiante.find()
    res.json(estudiantes);
}
estudianteCtrl.createEstudiantes = async (req,res) => {
    const estudiante = new Estudiante ({
        nombre: req.body.nombre,
    });
    await estudiante.save();
    res.json('Guardado');
}
estudianteCtrl.getEstudiante = async (req,res) => {
    const estudiante = await Estudiante.findById(req.params.id);
    res.json(estudiante);
}

estudianteCtrl.editEstudiante = async (req,res) => {
    const estudiante = {
        nombre: req.body.nombre,
    }
    await Estudiante.findByIdAndUpdate(req.params.id, {$set : estudiante});
    res.json('Actualizado');
}
estudianteCtrl.deleteEstudiante = async (req,res) => {
    await Estudiante.findByIdAndRemove(req.params.id)
    res.json('Eliminado')
}



module.exports = estudianteCtrl;