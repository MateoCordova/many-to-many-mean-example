const Materia = require('../Models/materia');
const Estudiante = require('../Models/estudiante');
const EstudianteMateria = require('../Models/estudianteMateria');

const materiaCtrl = {};

materiaCtrl.getMaterias = async (req,res) => {
    const materias = await Materia.find()
    res.json(materias);
}
materiaCtrl.createMaterias = async (req,res) => {
    const materia = new Materia ({
        nombre: req.body.nombre,
    });
    await materia.save();
    res.json('Guardado');
}
materiaCtrl.getMateria = async (req,res) => {
    const materia = await Materia.findById(req.params.id);
    res.json(materia);
}

materiaCtrl.editMateria = async (req,res) => {
    const materia = {
        nombre: req.body.nombre,
    }
    await Materia.findByIdAndUpdate(req.params.id, {$set : materia});
    res.json('Actualizado');
}
materiaCtrl.deleteMateria = async (req,res) => {
    await Materia.findByIdAndRemove(req.params.id)
    res.json('Eliminado')
}

materiaCtrl.getNotas = async (req,res) => {
    const x = await EstudianteMateria.find();
    console.log(x)
    res.json(x)
}

materiaCtrl.addNota = async (req,res) => {
    console.log(req.body);
    const materiadb = await Materia.findById(req.body.materia_id);
    console.log(materiadb);
    const estudiantedb = await Estudiante.findById(req.body.estudiante_id);
    console.log(estudiantedb);
    const estudianteMateria = new EstudianteMateria({
        materia_id : materiadb,
        estudiante_id: estudiantedb,
        nota: req.body.nota
    });
    await estudianteMateria.save();
    res.json('Guardado');
}

materiaCtrl.delNota = async (req,res) => {
    await EstudianteMateria.deleteOne({ '_id': req.params.id },function(err, result) {
        if (err) {
          console.log(err);
        } else {
          console.log(result);
        }
    });
    res.json('Eliminado');
}


module.exports = materiaCtrl;