const mongoose = require('mongoose');
const {Schema} = mongoose;

const EstudianteSchema = new Schema({
    //Materia: {type: mongoose.Schema.Types.ObjectId, ref:'Materia'},
    nombre: {type : String, requiere: true},
},{
    timestamps:true
});;

module.exports = mongoose.model('Estudiante', EstudianteSchema);