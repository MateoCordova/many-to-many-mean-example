const mongoose = require('mongoose');
const {Schema} = mongoose;

const EstudianteMateriaSchema = new Schema({
    materia_id: {type: mongoose.Schema.Types.ObjectId, ref:'materia'},
    estudiante_id: {type: mongoose.Schema.Types.ObjectId, ref:'estudiante'},
    nota: {type : Number, requiere: true},
},{
    timestamps:true
});;

module.exports = mongoose.model('EstudianteMateria', EstudianteMateriaSchema);