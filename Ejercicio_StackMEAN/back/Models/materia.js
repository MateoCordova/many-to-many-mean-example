const mongoose = require('mongoose');
const {Schema} = mongoose;

const MateriaSchema = new Schema({
    //Estudiante: {type: mongoose.Schema.Types.ObjectId, ref:'User'},
    nombre: {type : String, requiere: true},
},{
    timestamps:true
});;

module.exports = mongoose.model('Materia', MateriaSchema);