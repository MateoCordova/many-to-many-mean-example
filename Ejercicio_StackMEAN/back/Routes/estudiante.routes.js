const express = require('express');
const router = express.Router();
const estudianteCtrl = require('../Controllers/estudiante.controller');

router.get('/', estudianteCtrl.getEstudiantes);
router.post('/', estudianteCtrl.createEstudiantes);
router.get('/:id', estudianteCtrl.getEstudiante);
router.put('/:id', estudianteCtrl.editEstudiante);
router.delete('/:id', estudianteCtrl.deleteEstudiante);

module.exports = router;