const express = require('express');
const router = express.Router();
const materiaCtrl = require('../Controllers/materia.controller');

router.get('/', materiaCtrl.getMaterias);
router.post('/', materiaCtrl.createMaterias);
//router.get('/:id', materiaCtrl.getMateria);
router.put('/:id', materiaCtrl.editMateria);
router.delete('/:id', materiaCtrl.deleteMateria);

router.get('/notas', materiaCtrl.getNotas);
router.post('/notas', materiaCtrl.addNota);
router.delete('/notas/:id', materiaCtrl.delNota);

module.exports = router;

