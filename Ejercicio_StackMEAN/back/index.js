const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();
const {mongoose} = require('./database');

//Configuracion de servidor
app.set('port', process.env.PORT || 3000);


//Middleware
app.use(morgan('dev'));
app.use(express.json());
app.use(cors({origin: 'http://localhost:4200'}));

//Route
app.use('/api/materias',require('./Routes/materia.routes'));
app.use('/api/estudiantes',require('./Routes/estudiante.routes'));

//Prender servidor
app.listen(app.get('port'), () => {
    console.log('Server on port',app.get('port'));
});