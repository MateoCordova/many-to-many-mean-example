import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Components

import {EstudiantesComponent} from '../components/estudiantes/estudiantes.component';
import {MateriasComponent} from '../components/materias/materias.component';


const routes: Routes = [
  {
    path: 'Estudiantes',
    component: EstudiantesComponent,
  },
  {
    path: 'Materias',
    component: MateriasComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

