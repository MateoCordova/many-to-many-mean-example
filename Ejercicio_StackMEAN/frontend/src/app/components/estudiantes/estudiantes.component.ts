import { Component, OnInit } from '@angular/core';

import { EstudianteService} from '../../services/estudiante.service';
import { Form, NgForm } from '@angular/forms';
import { Estudiante } from 'src/app/models/estudiante';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html',
  styleUrls: ['./estudiantes.component.css']
})
export class EstudiantesComponent implements OnInit {

  constructor(public EstudianteService: EstudianteService ) { }

  ngOnInit(): void {
    this.getEstudiantes();
  }
  addEstudiante(form: NgForm):void {
    if(this.EstudianteService.selectedEstudiante._id){
      this.EstudianteService.putEstudiante(form.value).subscribe(res=>{
        this.resetForm(form);
        this.getEstudiantes();
      });
    } else {
      console.log(form.value)
      this.EstudianteService.postEstudiantes(form.value)
      .subscribe(res =>{
        this.resetForm(form);
        this.getEstudiantes();
      });
    }
  }
  resetForm(form?: NgForm){
    if(form){
      form.reset();
      this.EstudianteService.selectedEstudiante = new Estudiante();
    }
  }
  getEstudiantes(){
    this.EstudianteService.getEstudiantes().subscribe(res =>{
      this.EstudianteService.estudiantes = res as Estudiante[];
    });
  }
  editEstudiante(Estudiante:Estudiante){
    this.EstudianteService.selectedEstudiante = Estudiante;
  }
  deleteEstudiante(Estudiante:Estudiante){
    this.EstudianteService.deleteEstudiante(Estudiante._id).subscribe(res=>{
      this.resetForm();
      this.getEstudiantes();
    })
  }
}