import { Component, OnInit } from '@angular/core';

import { MateriaService} from '../../services/materia.service';
import { Form, NgForm } from '@angular/forms';
import { Materia } from 'src/app/models/materia';
import { ThrowStmt } from '@angular/compiler';

import {Nota} from 'src/app/models/nota'
import {Estudiante} from 'src/app/models/estudiante'
import {EstudianteService} from '../../services/estudiante.service'


@Component({
  selector: 'app-materias',
  templateUrl: './materias.component.html',
  styleUrls: ['./materias.component.css']
})
export class MateriasComponent implements OnInit {
  
  notaForm : Nota;
  notas : Nota[];

  constructor(public MateriaService: MateriaService, public EstudianteService: EstudianteService ) { }

  ngOnInit(): void {
    this.notaForm = new Nota();
    this.getMaterias();
    this.getEstudiantes();
  }
  addMateria(form: NgForm):void {
    if(this.MateriaService.selectedMateria._id){
      this.MateriaService.putMateria(form.value).subscribe(res=>{
        this.resetForm(form);
        this.getMaterias();
      });
    } else {
      console.log(form.value)
      this.MateriaService.postMaterias(form.value)
      .subscribe(res =>{
        this.resetForm(form);
        this.getMaterias();
      });
    }
  }
  resetForm(form?: NgForm){
    if(form){
      form.reset();
      this.MateriaService.selectedMateria = new Materia();
    }
  }
  getMaterias(){
    this.MateriaService.getMaterias().subscribe(res =>{
      this.MateriaService.materias = res as Materia[];
    });
  }
  editMateria(Materia:Materia){
    this.MateriaService.selectedMateria = Materia;
    this.getNotas();
  }
  deleteMateria(Materia:Materia){
    this.MateriaService.deleteMateria(Materia._id).subscribe(res=>{
      this.resetForm();
      this.getMaterias();
    })
  }
  //
  getEstudiantes(){
    this.EstudianteService.getEstudiantes().subscribe(res =>{
      this.EstudianteService.estudiantes = res as Estudiante[];
    });
  }
  getNotas(){
    this.MateriaService.getNotas().subscribe(res =>{
      this.notas = res as Nota[];
    });
  }
  
  addNota(){
    this.notaForm.materia_id = this.MateriaService.selectedMateria._id; //
    this.MateriaService.addNota(this.notaForm).subscribe(res =>{
      this.notaForm= new Nota();
      this.editMateria(this.MateriaService.selectedMateria)

    });
  }
  deleteNota(nota:Nota){
    this.MateriaService.delNota(nota).subscribe(res =>{
      this.editMateria(this.MateriaService.selectedMateria)
    });
  }
  isNotNull(){
    let flag = true;
    if(this.MateriaService.selectedMateria._id === undefined){
      flag = false;
    }
    return flag;
  }
  checkMateria(estudiante:Estudiante){
    let flag = false;
    this.notas.forEach(element => {
      if(element.estudiante_id === estudiante._id && element.materia_id === this.MateriaService.selectedMateria._id){
        flag = true;
      }
    });
    return flag;
  }
  promedioEstudiante(estudiante:Estudiante){
    let promedio = 0;
    let n = 0;
    this.notas.forEach(nota => {
      if(nota.materia_id==this.MateriaService.selectedMateria._id && nota.estudiante_id === estudiante._id){
        promedio += nota.nota;
        n++;
      }
    });
    return promedio/n;
  }
  promedio(){
    let promedio = 0;
    let promedioAux = 0;
    let n = 0;
    let nAux = 0;
    this.notas.forEach(notaLista => {
      this.EstudianteService.estudiantes.forEach(element => {
        if(notaLista.materia_id===this.MateriaService.selectedMateria._id && element._id === notaLista.estudiante_id){
          console.log(notaLista.nota);
          promedioAux += notaLista.nota;
          console.log(promedioAux);
          nAux++;
        }
      });
      if(nAux!== 0){
        promedio += promedioAux/nAux;
        n++;
        promedioAux = 0;
        nAux=0;
      }
      
      
    });
    return promedio/n;
  }
}