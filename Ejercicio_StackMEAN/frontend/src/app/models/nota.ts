export class Nota {
    _id: string;
    estudiante_id: string;
    materia_id: string;
    nota: number;
}
