import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Estudiante } from '../models/estudiante';

@Injectable({
  providedIn: 'root'
})
export class EstudianteService {

  selectedEstudiante: Estudiante;
  estudiantes: Estudiante[];
  readonly URL_API = 'http://localhost:3000/api/estudiantes';
  constructor(private http: HttpClient) {
    this.selectedEstudiante = new Estudiante();
   }

  getEstudiantes(){
    return this.http.get(this.URL_API);
  }

  postEstudiantes(Estudiante : Estudiante){
    return this.http.post(this.URL_API, Estudiante);
  }

  putEstudiante(Estudiante : Estudiante){
    return this.http.put(this.URL_API + `/${Estudiante._id}`,Estudiante);
  }
  deleteEstudiante(_id: string){
    return this.http.delete(this.URL_API + `/${_id}`)
  };
  
}
