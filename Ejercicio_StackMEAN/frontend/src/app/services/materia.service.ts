import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Materia } from '../models/materia';
import { Nota } from '../models/nota';


@Injectable({
  providedIn: 'root'
})
export class MateriaService {

  selectedMateria: Materia;
  materias: Materia[];
  readonly URL_API = 'http://localhost:3000/api/materias';
  constructor(private http: HttpClient) {
    this.selectedMateria = new Materia();
   }

  getMaterias(){
    return this.http.get(this.URL_API);
  }

  postMaterias(Materia : Materia){
    return this.http.post(this.URL_API, Materia);
  }

  putMateria(Materia : Materia){
    return this.http.put(this.URL_API + `/${Materia._id}`,Materia);
  }
  deleteMateria(_id: string){
    return this.http.delete(this.URL_API + `/${_id}`)
  };
  
  getNotas(){
    return this.http.get(this.URL_API+"/notas");
  }

  addNota(nota : Nota){
    console.log(nota);
    return this.http.post(this.URL_API + `/notas/`, nota);
  };
  delNota(nota : Nota){
    return this.http.delete(this.URL_API + `/relacion/${nota._id}`)
  };

}
