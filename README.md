# Many to many Mean Example

This project is an example of how to handle many to many relationships in stack MEAN

# Previous Steps explained (CRUD):

CRUD #1 https://www.youtube.com/watch?v=khCIunNAEHI&t=1740s
CRUD #2 https://www.youtube.com/watch?v=ccBtSAMFjto

ROUTING AND SECURITY #1 https://www.youtube.com/watch?v=rPD0eY3dRqQ&t=8s
ROUTING AND SECURITY #2 https://www.youtube.com/watch?v=l_r9nRJ9YTk&t


# Backend

Step 1 CREATE MONGOOSE MODEL FOR relationship ------------     estudianteMateria.js

Step 2 ADD FUNCTIONALITY IN CONTROLLER -------------------     materia.controller.js

Step 3 ADD ROUTES ----------------------------------------     materia.routes.js

# Frontend

STEP 1 GENERATE RELATIONSHIP CLASS -----------------------     ng g class notas

STEP 2 ADD METHODS IN SERVICE TO CALL RELATIONSHIP DATA --     materia.service.js 

STEP 3 CALL METHODS AND CREATE FLOW CONTROL METHODS ------     materia.component.ts

STEP 4 CREATE FUNCTIONALITY METHODS ----------------------     materia.component.ts

STEP 5 USE METHODS IN COMPONENT LAYOUT -------------------     materia.component.html